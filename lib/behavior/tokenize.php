<?php

class Tokenize extends SfPropelBehaviorBase
{
  protected $parameters = array(
    'token_column' => 'token',
    'token_largo' => '40',
  );

  public function preInsert()
  {
    if ($this->isDisabled())
    {
      return;
    }

    if ($column = $this->getParameter('token_column'))
    {
      return $this->tokenizeString();
    }
  }

  public function preSave()
  {
    if ($this->isDisabled())
    {
      return;
    }

    if ($column = $this->getParameter('token_column'))
    {
      return $this->tokenizeString();
    }
  }

  public function tokenizeString(){
    $column = $this->getParameter('token_column');
    $columnName = $this->getTable()->getColumn($column)->getPhpName();
    $tableName = $this->getTable()->getPhpName();
    $token_largo = $this->getParameter('token_largo');

  $string = " \$encontrado = true;
  while( \$encontrado ){
    \$token = substr( sha1( time().rand() ) , 0 , {$token_largo} );
    \$encontrado = {$tableName}Query::create()->filterBy{$columnName}(\$token)->findOne();
  }
  \$this->set{$columnName}( \$token );
  ";
      return $string;
  }

}