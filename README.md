# TokenizePlugin para Propel 1.6
##### Autor : [José Luis González](http://bitbucket.org/arkangelkruel)


Agregar en propel.ini
==================
```propel.behavior.tokenize.class = plugins.TokenizePlugin.lib.behavior.tokenize```

Para usar, agregar en schema.xml
=======================================
```xml
<column name="token" type="VARCHAR" size="255" required="true" />
<behavior name="tokenize">
  	<parameter name="token_column" value="token" />
  	<parameter name="token_largo" value="40" />
</behavior>
```
###### Largo máximo : 40


Ejecutar en consola
========================
```
php symfony cc
php symfony propel:build-model && php symfony propel:build-sql && php symfony propel:build-forms && php symfony propel:build-filters
```
